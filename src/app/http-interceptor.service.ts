import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { finalize, tap } from 'rxjs/operators';
import { LoaderService } from './services/loader.service';
import { LocalStorageService } from './services/local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

  constructor(
    private localStorageService :LocalStorageService,
    private loaderService:LoaderService
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.localStorageService.token;
    if (token) {
      req = req.clone({
        setHeaders: {
          Authorization: `Token ${token}`,
          'Accept-Language': 'fa-IR'
        }
      });
    }

   this.loaderService.showLoader()
    return next.handle(req).pipe(
      finalize(() => this.loaderService.hideLoader()),
      tap(
        event => {   
          console.log(event)
        },
        error => {
          alert(error.error.error)
        }
      )
    );
    
  }
}