import { Pipe, PipeTransform } from '@angular/core';
import { LocalStorageService } from '../services/local-storage.service';

@Pipe({
  name: 'translate'
})
export class TranslatePipe implements PipeTransform {
  constructor(
    private localStorageService:LocalStorageService
  ) { }
 languages :any  ={
      'fa-IR':{
        hello:"سلام",
        wellcome:"خوش آمدید",
        get_value:"لطفا سن خود را وارد کنید ",
        submit:"ثبت"
      },
      'en-US':{
        hello:"hello",
        wellcome:"wellcome",
        get_value:"please enter your age",
        submit:"submit"
      }
    }
  transform(value: string, ...args: unknown[]): unknown {
    let language:any='en-US';
    if(this.localStorageService.language){
      language=this.localStorageService.language;
    }
    return this.languages[language][value];
  }
}
