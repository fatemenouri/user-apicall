import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  subjectA: Subject<boolean> = new Subject();
  counter:number=0;

  constructor() { }
  
  showLoader(){ 
    this.counter=this.counter+1;
    if(this.counter > 0){
      this.subjectA.next(true)
    }
  }
  hideLoader(){
    this.counter=this.counter-1
    if(this.counter <= 0){
      this.subjectA.next(false)
    }
  }

}
