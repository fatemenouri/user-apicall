import { NgModule } from '@angular/core';
import { ActivatedRoute, RouterModule, Routes } from '@angular/router';
import { Page404Component } from './components/page404/page404.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { AuthGuardService } from './services/auth-guard.service';
import { ResolverService } from './services/resolver.service';

const routes: Routes = [
  {
    path: '',
    component: WelcomeComponent,
    data: ['Ali', 'Saeed']
  },
  {
    path: 'category/:id',
    component: ActivatedRoute
  },
  {
    path: 'category/:id/:name',
    component: ActivatedRoute
  },
  {
    path: 'portfolio',
    loadChildren: () => import('./modules/portfolio/portfolio.module').then(mod => mod.PortfolioModule)
  },
  {
    path: 'profile',
    component: ActivatedRoute,
    canActivate: [AuthGuardService],
    resolve: [ResolverService]
  },
  {
    path: '404',
    component: Page404Component
  },
  {
    path: '**',
    redirectTo: '/404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
